#! bin/bash
fileline=$(mocp -i | grep File)
if [ ! -z "$fileline" ] 
then
	echo $(basename -s $fileline)
else
	echo ""
fi
