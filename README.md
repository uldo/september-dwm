# September dwm

1. [Dependencies](### Dependencies or something)
2. [Installation](###Installation)
3. [Keybindings](### Keybindings)
4. [Cool Screenshots](### Cool screenshots)


### Dependencies or something
you will definitely need base-devel and yajl for dwm, everything in list is optional, but highly recommended if you don't want to tweak anything

- picom (for transparency and such)
- xbindkeys (for keyboard shortcuts)
- maim (for making screenshots to post on r/unixporn)
- feh (for background)
- moc (for playing music)
- xclip (for clipboard)

also you will need [polybar-dwm-module](https://github.com/mihirlad55/polybar-dwm-module) (if you are on arch, you can download it from aur) and [xgetres](https://github.com/tamirzb/xgetres) if you want really handy dmenu script to work

### Installation
be sure to backup all your important dotfiles like .xinitrc and .Xresources before installing if you need them, same with dwm st dmenu an tabbed configurations

copy all files (exept for README.md if you really don't want to) to your home folder
then in `.config/dwm-6.2/`   run 

    $ sudo make clean install

then, do the same for these folders in .config
    - dmenu
    - st-0.8.4
    - tabbed-0.6

and that is pretty much it, run with `$ startx` and have fun!

### Keybindings
##### dwm
    win + n - next tag
    win + b - previous tag
    win + shift + f - fullscreen
    alt + enter - spawn st without tabs
    win + shift + w - toggle floating, because I prefer win + space to toggle keyboard layouts
##### tabbed
    to show and do something with tabbs hold alt + shift, everything else is default
##### st
    to scroll use shift + mousewheel or shift + pg up/down
##### other 
    print - take screenshot with maim, copy to clipboard with xclip and save in ~/Pictures/Screenshots
    ctrl + print - same but you take screenshot of region instead of full screen
    win + shift + p - dmenu script, which shows only something that you have alredy runed with it (also can run mocp and ranger in terminal)

### Cool screenshots
![screenshot](https://i.redd.it/60mjl3yrzfq71.png)

